# ===========================================================================

# This script implements a set of functions to draw the divisibility graph

# @author: Luca Piovano
# @date: March 02, 2019

# More infromation and references: https://datarium.netlify.com/posts/190303_the_divisibility_graph/

# ===========================================================================

library( dplyr )                  # to manipulate dataframes
library( ggplot2 )                # to provide the drawing environment
library( ggraph )                 # to draw the graph
library( igraph )                 # to initialize a graph object

# ===========================================================================
# Define the theme of the plot
# ===========================================================================

unit_links = 'black'
ten_links = '#cc3600'
txt = 'black'
label_txt = 'white'
label_background = '#1a1a1a'
background = 'white'

fontFamily = 'Liberation Sans Narrow'

themePlot = theme( legend.position  = "none",
                   plot.background  = element_rect( fill = background, color = NA ),
                   panel.background = element_rect( fill = background, color = NA ),
                   axis.ticks       = element_blank(),
                   panel.grid       = element_blank(),
                   axis.title       = element_blank(),
                   axis.text        = element_blank(),
                   plot.title       = element_text( family = fontFamily, face = 'bold', colour = txt, 
                                                    size = 15, hjust = 0.5, vjust = 0.5 ),
                   plot.subtitle    = element_text( family = fontFamily, face = 'italic', colour = txt, 
                                                    size = 13, hjust = 0.5, vjust = 0.5 ),
                   plot.caption     = element_text( face = 'bold.italic', size = 8, colour = txt ) )

# ==============================================================================================
# Prepare the data.frame with the list of the links (starting and ending points) and their type
# ==============================================================================================

# The unity links make a ring among all the values in [0..divisor - 1]
# The tens links represent shortcomings in the path to follow and they are computed like this:
#                 (10 * [0..divisor -1]) mod divisor

computeDivisibilityDF = function( divisor ) {
  return ( data.frame( start_node = seq( 0, divisor - 1, by = 1 ),
                       end_node = seq( 1, divisor, by = 1 ) %% divisor ) %>%
             bind_rows( data.frame( start_node = seq( 0, divisor - 1, by = 1 ), 
                                    end_node = ( 10 * seq( 0, divisor - 1, by = 1 ) ) %% divisor ) ) %>%
             mutate( link_type = c( rep( 'units', divisor ), rep( 'tens', divisor ) ) )  %>%
             filter( start_node != end_node ) )
}

# ===========================================================================================
# Draw the graph
# ===========================================================================================

plotDivisibilityGraph = function( divisor, label_text = 'white', label_bg = 'black',
                                  title = '', subtitle = '', caption = '' ) {
  # compute the divisibility data frame
  divisibility_df = computeDivisibilityDF( divisor )
  
  # compute the graph
  graph = graph_from_data_frame( divisibility_df )
  
  plot_graph = ggraph( graph, layout = 'linear', circular = TRUE ) + 
    geom_edge_fan( aes( colour = factor( link_type ) ), 
                   arrow = arrow( length = unit( 5 - log( divisor ), 'mm' ), ends = 'last', type = 'closed' ), 
                   start_cap = circle( 7 - log( divisor ), 'mm' ), end_cap = circle( 7 - log( divisor ), 'mm' ) ) + 
    geom_node_label( aes( label = seq( 0, divisor - 1 ) ), label.r = unit( 0.5, 'lines' ), label.padding = unit( 0.3, 'lines' ),
                     size = 8 - log( divisor ), label.size = 1 / log( divisor ), colour = label_text, fill = label_bg,
                     fontface = 'bold' ) +
    scale_edge_color_manual( values = c( 'units' = unit_links, 'tens' = ten_links ) ) + 
    coord_fixed() + 
    themePlot + 
    labs( title = title, subtitle = subtitle, caption = caption )
  
  return ( plot_graph )  
}

# get the divisibility graph and plot it
div_seq = seq( 2, 100 )
img_dirPath = 'img'

for ( i in div_seq ) {
  plot_graph = plotDivisibilityGraph( i, label_text = label_txt, label_bg = label_background, caption = 'Datarium - 2019' )
  plot_graph
  
  # ===========================================================================================
  # Save the image
  # ===========================================================================================
  
  ggsave( filename = paste0( 'divGraph_', i, '.png' ), plot = plot_graph, path = img_dirPath, dpi = 350 )
}
